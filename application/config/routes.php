<?php

if(FULL_PATH == '') {
    return [
        '' => [
            'controller' => 'auth',
            'action' => 'index'
        ],
        'singup' => [
            'controller' => 'auth',
            'action' => 'showSingUp'
        ],
        'singin' => [
            'controller' => 'auth',
            'action' => 'showSingIn'
        ],
        'login' => [
            'controller' => 'auth',
            'action' => 'login'
        ],
        'register' => [
            'controller' => 'auth',
            'action' => 'register'
        ],
        'logout' => [
            'controller' => 'auth',
            'action' => 'logout'
        ],
        'admin' => [
            'controller' => 'admin',
            'action' => 'index'
        ],
        'page' => [
            'controller' => 'page',
            'action' => 'index'
        ],
        'create-page' => [
            'controller' => 'page',
            'action' => 'create'
        ],
        'delete-page' => [
            'controller' => 'page',
            'action' => 'delete'
        ],
        'timetable' => [
            'controller' => 'timetable',
            'action' => 'index'
        ],
        'show-create-form' => [
            'controller' => 'timetable',
            'action' => 'showCreateForm'
        ],
        'create-courier-route' => [
            'controller' => 'timetable',
            'action' => 'createCourierRoute'
        ],
    ];
}
else {
    $path = substr(FULL_PATH, 1);
    return [
        $path.'' => [
            'controller' => 'auth',
            'action' => 'index'
        ],
        $path.'/singup' => [
            'controller' => 'auth',
            'action' => 'showSingUp'
        ],
        $path.'/singin' => [
            'controller' => 'auth',
            'action' => 'showSingIn'
        ],
        $path.'/login' => [
            'controller' => 'auth',
            'action' => 'login'
        ],
        $path.'/register' => [
            'controller' => 'auth',
            'action' => 'register'
        ],
        $path.'/logout' => [
            'controller' => 'auth',
            'action' => 'logout'
        ],
        $path.'/admin' => [
            'controller' => 'admin',
            'action' => 'index'
        ],
        $path.'/page' => [
            'controller' => 'page',
            'action' => 'index'
        ],
        $path.'/create-page' => [
            'controller' => 'page',
            'action' => 'create'
        ],
        $path.'/delete-page' => [
            'controller' => 'page',
            'action' => 'delete'
        ],
        $path.'/timetable' => [
            'controller' => 'timetable',
            'action' => 'index'
        ],
        $path.'/show-create-form' => [
            'controller' => 'timetable',
            'action' => 'showCreateForm'
        ],
        $path.'/create-courier-route' => [
            'controller' => 'timetable',
            'action' => 'createCourierRoute'
        ],
    ];
}

