<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

	    define('HOST', '127.0.0.1');
	    define('DB', 'codeit');
	    define('USER', 'root');
	    define('PASS', '');
	    define('CHARSET', 'utf8');
	    define('VIEWS_PATH', 'application/views');
	    define('PUBLIC_PATH', '/public');
	    define('IMAGE_PATH', '/public/images/');
	    define('FULL_PATH', '');
	    define('FILES_PATH', '');

function dump($str) {
	echo "<pre style='background-color:pink; padding: 10px;'>";
	var_dump($str);
	echo "</pre>";
}

function dd($str) {
	echo "<pre style='background-color:pink; padding: 10px;'>";
	var_dump($str);
	echo "</pre>";
	die();
}

function fileExist($file) : bool
{
    $fileIsExist = !empty(array_filter(scandir('application/views/page/users-pages/'), function ($page) use ($file) {
        return $page == $file . '.php';
    }));

    return (bool) $fileIsExist;
}

function trimDate($string)
{
    $string[10] = ' ';

    return $string;
}

