<?php
namespace application\validators;

use application\core\Validator;

class TimetableValidator extends Validator
{
    public function validate($postData)
    {
        $isValid = true;

        if (!empty($postData['courier_id']) && !empty($postData['destination_id']) && !empty($postData['start_datetime']) && !empty($postData['end_datetime'])) {

            if (!$this->isAllowableSize($postData['courier_id'], 1)) {
                $isValid = false;
            }

            if (!$this->isAllowableSize($postData['destination_id'], 1)) {
                $isValid = false;
            }

            if (!$this->isCorrectDatetimeFormat($_POST['start_datetime']) && !$this->isCorrectDatetimeFormat($_POST['end_datetime'])) {
                $isValid = false;
            }

            if (!$this->isCorrectDatetimeSequence($_POST['start_datetime'], $_POST['end_datetime'])) {
                $isValid = false;
            }
        } else {
            $isValid = false;
        }

        return $isValid;
    }
}