<?php

namespace application\validators;

use application\core\Validator;

class PageValidator extends Validator
{
    public function validate($postData)
    {
        $isValid = true;

        if (!empty($postData['title']) && !empty($postData['description']) && !empty($postData['content']) && !empty($postData['desired_url'])) {

            if (!$this->isAllowableSize($postData['title'], 3, 20)) {
                $isValid = false;
            }

            if (!$this->isOnlyLettersAndOrDigits($postData['title'])) {
                $isValid = false;
            }

            if (!$this->isAllowableSize($postData['description'], 3, 200)) {
                $isValid = false;
            }

            if (!$this->isOnlyLettersAndOrDigits($postData['description'])) {
                $isValid = false;
            }

            if (!$this->isAllowableSize($postData['desired_url'], 3, 20)) {
                $isValid = false;
            }

            if (!$this->isOnlyLettersAndOrDigits($postData['desired_url'])) {
                $isValid = false;
            }

            if (!$this->isUnique($postData['desired_url'])) {
                $isValid = false;
            }

            if ($this->htmlHasScripts($postData['content'])) {
                $isValid = false;
            }
        } else {
            $isValid = false;
        }

        return $isValid;
    }
}