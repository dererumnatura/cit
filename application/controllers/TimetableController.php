<?php

namespace application\controllers;


use application\core\Controller;
use application\models\Courier;
use application\models\Destination;
use application\models\Timetable;
use application\validators\TimetableValidator;

class TimetableController extends Controller
{
    private $couriers;
    public $validator;
    private $destinations;
    private $timetable;

    public function __construct($route, array $url_params = [], array $post_vars = [])
    {
        parent::__construct($route, $url_params, $post_vars);

        $this->couriers = new Courier();
        $this->validator = new TimetableValidator();
        $this->destinations = new Destination();
        $this->timetable = new Timetable();
    }

    public function index()
    {
        $records = $this->timetable->get();
        $errors = '';

        if (isset($_GET['searched-dt']) && !empty($_GET['searched-dt'])) {
            $records = $this->timetable->getFilteredDateTime($_GET['searched-dt']);
        }

        return $this->view->render('Couriers timetable', ['records' => $records, 'errors' => $errors]);
    }

    public function showCreateForm()
    {
        $errors = '';

        if (!empty($_GET['e']) && isset($_GET['e']) && $_GET['e'] == 'isbusy') {
            $errors = 'Current courier is busy.';
        }

        if (!empty($_GET['e']) && isset($_GET['e']) && $_GET['e'] == 'e') {
            $errors = 'Data is not valid.';
        }

        return $this->view->render('Create timetable route', [
            'couriers' => $this->couriers->get(),
            'destinations' => $this->destinations->get(),
            'errors' => $errors
        ]);
    }

    public function createCourierRoute()
    {
        if ($this->validator->validate($_POST)) {
            if ($this->timetable->courierIsBusy($_POST['start_datetime'], $_POST['end_datetime'], (int)$_POST['courier_id'])) {
                return $this->view->redirect('show-create-form?e=isbusy');
            }

            $this->timetable->createRoute(array_keys($_POST), array_values($_POST));

            return $this->view->redirect('timetable');
        }

        return $this->view->redirect('show-create-form?e=e');
    }
}