<?php

namespace application\controllers;

use application\core\Controller;
use application\validators\PageValidator;
use application\models\Pages;


class PageController extends Controller
{
    private $pages;
    private $validator;
    public $errors = [];

    public function __construct($route, array $url_params = [], array $post_vars = [])
    {
        parent::__construct($route, $url_params, $post_vars);

        if (!isset($_SESSION['login']) || empty($_SESSION['login'])) {
            header("location: /login]");
        }

        $this->view->layout = 'admin';
        $this->pages = new Pages();
        $this->validator = new PageValidator();
    }

    public function index()
    {
        if (isset($_GET['e'])) {
            return $this->view->render('Create pages', ['errors' => 'ffff']);
        }

        return $this->view->render('Create pages');
    }

    public function create()
    {
        if (!$this->validator->validate($_POST)) {
            return $this->view->redirect('page?e=1');
        }

        $this->pages->create($_POST);
        return $this->view->redirect('admin');
    }

    public function delete()
    {
        $this->pages->remove($_POST);

        return $this->view->redirect('admin');
    }
}