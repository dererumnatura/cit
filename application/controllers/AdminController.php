<?php

namespace application\controllers;

use application\core\Controller;
use application\models\Pages;

class AdminController extends Controller
{
    public $pages;

    public function __construct($route, array $url_params = [], array $post_vars = [])
    {
        parent::__construct($route, $url_params, $post_vars);

        if (!isset($_SESSION['login']) || empty($_SESSION['login'])) {
            header("location: /login");
        }

        $this->view->layout = 'admin';
        $this->pages = new Pages();
    }

    public function index()
    {
        $this->view->render('Admin', ['pages' => $this->getPages()]);
    }

    public function getPages()
    {
        return $this->pages->get();
    }
}