<section>
    <div class="weclome">
        <div class="container">
            <div class="create-page-form">
                <button type="button" class="btn btn-outline-primary"><a href="<?php echo FULL_PATH?>/show-create-form">Create courier
                        route</a></button>
            </div>
            <div class="timetable-filter">
                <form action="<?php echo FULL_PATH?>/timetable" method="GET">
                    <label for="searched-dt">Search by day/time:</label>
                    <input id="searched-dt" type="datetime-local" name="searched-dt" value="<?php echo !empty($_GET['searched-dt']) ? $_GET['searched-dt'] : NULL?>">
                    <button type="submit" class="btn btn-warning">Filter</button>
                </form>
            </div>
            <div class="couriers-timetable">
                <div class="timetable-header">
                    <p>Time</p>
                    <p>Courier name</p>
                    <p>Destination</p>
                    <p>Date</p>
                </div>
                <?php foreach ($vars['records'] as $record): ?>
                    <div class="courier">
                        <div class="c-time">
                            <p>Start time: <?php echo date("H:i", strtotime($record['start_datetime'])) ?></p>
                            <p>End time: <?php echo date("H:i", strtotime($record['end_datetime'])) ?></p>
                        </div>
                        <div class="c-text-data">
                            <p><?php echo $record['courier'] ?></p>
                            <p>
                                <?php echo $record['destination']['d_city'] . ', '?>
                                <?php echo $record['destination']['d_country'] ?>
                            </p>
                        </div>
                        <div class="c-date">
                            <p>Start date: <?php echo date("d-m-Y", strtotime($record['start_datetime'])) ?></p>
                            <p>End date: <?php echo date("d-m-Y", strtotime($record['end_datetime'])) ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>