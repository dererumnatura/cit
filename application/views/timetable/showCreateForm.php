<section>
    <div class="weclome">
        <div class="container">
            <h2>Create courier route</h2>
<!--            --><?php //dump($vars) ?>
            <?php if (!empty($vars['errors'])) : ?>
                <p class="danger"><?php echo $vars['errors']?></p>
            <?php endif?>
            <div class="create-page-form">
                <form action="<?php echo FULL_PATH?>/create-courier-route" method="POST">
                    <div class="form-group courier-form">
                        <label for="courier">Select courier:</label>
                        <select id="courier" size="10" name="courier_id" required>
                            <?php foreach($vars['couriers'] as $courier):?>
                                <option value="<?php echo $courier['id'] ?>"><?php echo $courier['name'] ?></option>
                            <? endforeach; ?>
                        </select>
                        <label for="destination">Select destination:</label>
                        <select id="destination" size="10" name="destination_id" required>
                            <?php foreach($vars['destinations'] as $destination):?>
                                <option value="<?php echo $destination['id'] ?>"><?php echo $destination['d_country'] . ", " . $destination['d_city']?></option>
                            <? endforeach; ?>
                        </select>
                        <label for="start-dt">Select start date/time:</label>
                        <input id="start-dt" type="datetime-local" name="start_datetime" required>
                        <label for="end-dt">Select end date/time:</label>
                        <input id="end-dt" type="datetime-local" name="end_datetime" required>
                        <button type="submit" class="btn btn-outline-primary">Go</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>