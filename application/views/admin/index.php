<section>
    <div class="welcome">
        <div class="container">
            <?php if (!empty($vars['login'])) : ?>
                <h1>Welcome, <?php echo $vars['login'] ?></h1>
            <?php else: ?>
                <h1>Welcome, <?php echo $_SESSION['login'] ?></h1>
            <?php endif; ?>
            <button type="button" class="btn btn-outline-primary"><a href="<?php echo FULL_PATH?>/page">Create page</a></button>
            <div class="pages-list">
                <?php foreach ($vars['pages'] as $post): ?>
                    <div class="page">
                        <div class="page-info">
                            <h3><?php echo $post['title'] ?></h3>
                            <pre><?php echo $post['description'] ?></pre>
                        </div>
                        <div class="page-control-btns">
                            <button class="btn btn-success"><a href="<?php echo FULL_PATH?>/<?php echo $post['desired_url'] ?>">Visit page</a></button>
                            <form action="<?php echo FULL_PATH?>/delete-page" method="POST">
                                <input type="hidden" name="url_to_delete" value="<?php echo $post['desired_url'] ?>">
                                <button class="btn btn-outline-danger" type="submit">Delete page</button>
                            </form>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>