<section>
    <div class="weclome">
        <div class="container">
            <?php if (!empty($vars['errors'])) : ?>
                <p class="danger">Data are incorrect. Pleace, try again.</p>
            <?php endif?>
            <div class="create-page-form">
                <form action="<?php echo FULL_PATH?>/create-page" method="POST">
                    <div class="form-group">
                        <input type="text" placeholder="title" class="form-control" name="title" required>
                        <input type="text" placeholder="description" class="form-control" name="description" required>
                        <textarea type="text" placeholder="content" class="form-control" name="content" required></textarea>
                        <input type="text" placeholder="desired URL" class="form-control" name="desired_url" required>
                        <button type="submit" class="btn btn-primary">Go</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>