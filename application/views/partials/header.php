<header id="header"><!--header-->

    <div class="header-middle"><!--header-middle-->
        <nav class="navbar">
            <div class="container">
                <ul class="navbar-nav">
                    <li><a class="nav-link" href="<?php echo FULL_PATH ?>/"><i class="fa fa-lock"></i> Home</a></li>
                    <?php if (empty($_SESSION['login'])): ?>
                        <li><a class="nav-link" href="<?php echo FULL_PATH ?>/singin"><i class="fa fa-user"></i> Account</a></li>
                    <?php else: ?>
                        <li><a class="nav-link" href="<?php echo FULL_PATH ?>/admin"><i
                                        class="fa fa-user"></i><?php echo $_SESSION['login']; ?></a>
                        </li>
                    <?php endif ?>

                    <!-- Заменить кнопку и путь  на выход -->
                    <?php if (empty($_SESSION['login'])): ?>
                        <li><a class="nav-link" href="<?php echo FULL_PATH ?>/singin"><i class="fa fa-lock"></i> login</a></li>
                    <?php else: ?>
                        <li><a class="nav-link" href="<?php echo FULL_PATH ?>/logout"><i class="fa fa-lock"></i> logout</a></li>
                    <?php endif ?>
                    <li><a class="nav-link" href="<?php echo FULL_PATH ?>/timetable"><i class="fa fa-lock"></i> Timetable</a></li>
                </ul>
            </div>
        </nav>
    </div><!--/header-middle-->
</header><!--/header-->

