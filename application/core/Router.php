<?php

namespace application\core;

use application\lib\Db;

class Router
{
    protected $routes = [];
    protected $params = [];
    protected $main_url = '';
    protected $url_params = '';
    protected $post_vars = [];

    public function __construct()
    {
        $rts = require 'application/config/routes.php';

        foreach ($rts as $key => $value) {
            $this->add($key, $value);
        }

    }

    public function add($route, $params)
    {
        $route = '#^' . $route . '$#';
        $this->routes[$route] = $params;
    }

    public function match()
    {
        $this->processUrl($_SERVER['REQUEST_URI']);

        foreach ($this->routes as $route => $params) {


            if (preg_match($route, $this->main_url, $matches)) {
                $this->params = $params;
                return true;
            }
        }
        return false;
    }

    public function run()
    {
        if ($this->match()) {

            $path = 'application\controllers\\' . ucfirst($this->params['controller']) . 'Controller';
            if (class_exists($path)) {
                $action = $this->params['action'];

                if (method_exists($path, $action)) {
                    $controller = new $path($this->params, $this->url_params, $this->post_vars);
                    $controller->$action();

                } else {
                    echo $action . ' not found! <br/>LINE: ' . __LINE__ . '<BR> METHOD: ' . __METHOD__;
                }
            } else {
                echo $path . ' not found! <br/>LINE: ' . __LINE__ . '<BR> METHOD: ' . __METHOD__;
            }

        } else {
            if(!empty(FULL_PATH)) {
                $fileIsExist = !empty(array_filter(scandir('application/views/page/users-pages/'), function ($page) {
                    return 'test/' . $page == $this->main_url . '.php';
                }));
            }
            else {
                $fileIsExist = !empty(array_filter(scandir('application/views/page/users-pages/'), function ($page) {
                    return $page == $this->main_url . '.php';
                }));
            }

            if (!$fileIsExist) {
                echo 'page not found';
                return false;
            }

            $this->echoUsersPage();
        }
    }

    private function processUrl($url)
    {

        $s_url = trim($url, "/");
        $url = explode("/", $s_url);

        $main_url = parse_url($_SERVER['REQUEST_URI'])['path'];
        $main_url = trim($main_url, "/");


        $this->main_url = $main_url;

        $this->url_params = $this->sanitizeInput(array_slice($url, 2));


        if (!empty($_POST)) {
            $this->post_vars = $this->sanitizeInput($_POST);
        }


    }

    private function sanitizeInput($str)
    {

        if (is_array($str)) {
            $res = [];
            foreach ($str as $k => $s) {
                $res[$k] = trim($s);
                $res[$k] = strip_tags($s);
                $res[$k] = htmlspecialchars($res[$k]);
            }
            return $res;
        }

        $input_text = trim($str);
        $input_text = strip_tags($input_text);
        $input_text = htmlspecialchars($input_text);

        return $input_text;
    }

    private function echoUsersPage()
    {
        if(!empty(FULL_PATH)) {
            $path = str_replace(FILES_PATH, '', $this->main_url);

            $myfile = fopen("application/views/page/users-pages/". $path .".php", "r") or die("Unable to open file!");
            $text = fread($myfile,filesize("application/views/page/users-pages/". $path .".php"));
            echo htmlspecialchars_decode($text);
            fclose($myfile);
        }
        else {
            $myfile = fopen("application/views/page/users-pages/". $this->main_url .".php", "r") or die("Unable to open file!");
            $text = fread($myfile,filesize("application/views/page/users-pages/". $this->main_url .".php"));
            echo htmlspecialchars_decode($text);
            fclose($myfile);
        }
    }
}