<?php


namespace application\core;

use application\models\Pages;
use DateTime;

abstract class Validator
{
    private $pages;

    public function __construct()
    {
        $this->pages = new Pages();
    }

    public function isOnlyLettersAndOrDigits($string): bool
    {
        return preg_match("/^[a-zA-Z0-9]+$/", $string);
    }

    public function isOnlyLetters(string $string): bool
    {
        return preg_match("/^[a-zA-Z]+$/", $string);
    }

    public function isAllowableSize(string $string, int $minSize, $maxSize = ''): bool
    {
        return empty($maxSize) ? strlen($string) === $minSize : strlen($string) > $minSize && strlen($string) < $maxSize;
    }

    public function isUnique(string $string): bool
    {
        return !$this->pages->getOneBySlug($string);
    }

    public function htmlHasScripts($string): bool
    {
        preg_match_all("/(&lt;script)[a-z0-9¦>,;#!\"'()%~:_=&\-\.\?\/\s]*(&lt;\/script&gt;)/i", htmlspecialchars($string), $matches);
        preg_match_all("/(&lt;script)[a-z0-9¦>,;#!\"'()%~:_=&\-\.\?\/\s]*(&lt;\/script&gt;)/i", $string, $matchesNext);

        $isNotValid = true;
        foreach ($matches as $match) {
            if(!empty($match) && isset($match)) {
                $isNotValid = false;
            }
        }

        foreach ($matchesNext as $match) {
            if(!empty($match) && isset($match)) {
                $isNotValid = false;
            }
        }

        return !$isNotValid;
    }

    public function isCorrectDatetimeFormat($string): bool
    {
        return (bool) DateTime::createFromFormat('Y-m-d H:i', trimDate($string));
    }

    public function isCorrectDatetimeSequence($startDate, $endDate)
    {
        $sDate = DateTime::createFromFormat('Y-m-d H:i', trimDate($startDate));
        $eDate = DateTime::createFromFormat('Y-m-d H:i', trimDate($endDate));

        return $sDate < $eDate;
    }

    abstract public function validate($data);
}