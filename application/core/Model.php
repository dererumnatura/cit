<?php 

	namespace application\core;
	use \PDO;

	class Model extends PDO
    {
        public $pdo;

        public function __construct()
        {
            $dsn = "mysql:host=" . HOST . ";dbname=" . DB . ";charset=" . CHARSET;

            $opt = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => false,
            ];
            $pdo = new PDO($dsn, USER, PASS, $opt);
            $this->pdo = $pdo;
        }

        protected function createRecord($table, $columns = [], $values = [])
        {
            $sql = 'INSERT INTO ' . $table . '(' . implode(",", $columns) . ') VALUES  ( "' . implode('","', $values) . '")';
            $query = $this->pdo->prepare($sql);
            $query->execute();

            return $this->pdo->lastInsertId();
        }

        protected function updateRecord($table, $columns = [], $values = [], int $id)
        {
            $queryData = array_map(function ($el, $el2) {
               return $el . '="' . $el2 . '"';
            }, $columns, $values);

            $sql = 'UPDATE ' . $table . ' SET ' . implode(",", $queryData) . ' WHERE id=' . $id;
            $query = $this->pdo->prepare($sql);
            $query->execute();

            return $this->pdo->lastInsertId();
        }

        protected function deleteRecord($table, $condition)
        {
            $sql = 'DELETE FROM ' . $table . ' WHERE desired_url="' . $condition . '"' ;
            $query = $this->pdo->prepare($sql);
            $query->execute();

            return $this->pdo->lastInsertId();
        }

        protected function getAll($table, $columns = [], $condition = '', $condition2 = '', $condition3 = '')
        {
            if (empty($columns) && empty($condition) && empty($condition2) && empty($condition3)) {
                $query = $this->pdo->query('SELECT * FROM ' . $table);
            }

            if(!empty($condition) && !empty($columns)) {
                $query = $this->pdo->query( 'SELECT '. implode(", ", $columns) . ' FROM ' . $table . ' WHERE topic_id=' . $condition );
            }

            if(!empty($columns)) {
                $query = $this->pdo->query( 'SELECT '. implode(", ", $columns) . ' FROM ' . $table );
            }

            if(empty($columns) && !empty($condition)) {
                $query = $this->pdo->query( 'SELECT * FROM ' . $table . ' WHERE start_datetime <= "' . $condition . '" AND end_datetime >="' . $condition . '"');
            }

            if(empty($columns) && !empty($condition2)) {
                $query = $this->pdo->query( 'SELECT * FROM ' . $table . ' WHERE start_datetime BETWEEN "' . $condition . '" AND "' . $condition2 . '" OR end_datetime BETWEEN "' . $condition . '" AND "' . $condition2 . '" AND courier_id=' . $condition3);
            }

            $result = $query->fetchAll();

            return $result;
        }

//        protected function getAllTimetableRecords($courierId, $destinationId)
//        {
//            $query = $this->pdo->query( 'SELECT * FROM timetable WHERE courier_id=' . $courierId . ' AND destinationId=' . $destinationId );
//
//        }

        public function getOne($table, $id)
        {
            $query = $this->pdo->prepare('SELECT * FROM ' . $table . ' WHERE id = ' . $id);
            $query->execute();
            $result = $query->fetch();

            return $result;
        }

        public function getPageBySlug($table, $slug)
        {
            $query = $this->pdo->prepare('SELECT * FROM ' . $table . ' WHERE desired_url = "' . $slug . '"');
            $query->execute();
            $result = $query->fetch();

            return $result;
        }

        public function getLogin($table, $login)
        {
            $query = $this->pdo->prepare('SELECT * FROM ' . $table . ' WHERE login ="' . $login . '"');
            $query->execute();
            $result = $query->fetch();
            return $result;
        }


        public function getEmail($table, $email)
        {
            $query = $this->pdo->prepare('SELECT * FROM ' . $table . ' WHERE email ="' . $email . '"');
            $query->execute();
            $result = $query->fetch();
            return $result;
        }

        public function getPassword($table, $pass)
        {
            $query = $this->pdo->prepare('SELECT * FROM ' . $table . ' WHERE password ="' . $pass . '"');
            $query->execute();
            $result = $query->fetch();
            return $result;
        }

	}