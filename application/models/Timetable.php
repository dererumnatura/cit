<?php

namespace application\models;


use application\core\Model;

class Timetable extends Model
{
    protected $table = 'timetable';
    private $couriers;
    private $destinations;

    public function __construct()
    {
        parent::__construct();

        $this->couriers = new Courier();
        $this->destinations = new Destination();
    }

    public function get()
    {
        return $this->mapTimetableRecordsToResponse($this->getAll($this->table));
    }

    public function getFilteredDateTime($datetime)
    {
        return $this->mapTimetableRecordsToResponse($this->getAll($this->table, [], $datetime));
    }

    public function createRoute($values, $columns)
    {
        $this->createRecord($this->table, $values, $columns);
    }

    public function courierIsBusy($startDaytime, $endDaytime, $courierId)
    {
        return !empty($this->getAll($this->table, [], $startDaytime, $endDaytime, $courierId));
    }

    private function mapTimetableRecordsToResponse($records)
    {
        return array_map(function ($record) {
            return [
                'id' => $record['id'],
                'courier' => $this->couriers->getOnlyOne($record['courier_id'])['name'],
                'destination' => $this->destinations->getOnlyOne($record['destination_id']),
                'start_datetime' => $record['start_datetime'],
                'end_datetime' => $record['end_datetime']
            ];
        }, $records);
    }
}