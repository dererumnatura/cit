<?php

namespace application\models;


use application\core\Model;

class Courier extends Model
{
    private $table = 'couriers';

    public function get()
    {
        return $this->getAll($this->table);
    }

    public function getOnlyOne(int $id)
    {
        return $this->getOne($this->table, $id);
    }

}