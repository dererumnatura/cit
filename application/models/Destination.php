<?php

namespace application\models;


use application\core\Model;

class Destination extends Model
{
    private $table = 'destination';
    private $columns = ['id', 'd_city', 'd_country'];

    public function get()
    {
        return $this->getAll($this->table, $this->columns);
    }

    public function getOnlyOne($id)
    {
        return $this->getOne($this->table, $id);
    }
}