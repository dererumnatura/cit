<?php

namespace application\models;
use application\core\Model;

class Pages extends Model
{
    public $table = 'pages';
    private $columns = ['id', 'title', 'description', 'desired_url'];

    public function __construct()
    {
        parent::__construct();
    }

    public function create(array $data)
    {
        $testFile = fopen("application/views/page/users-pages/" . $data['desired_url'] . ".php", "w");
        $content = $data['content'];
        fwrite($testFile, $content);
        fclose($testFile);

        return $this->createRecord($this->table, array_keys($data), array_values($data));
    }

    public function get()
    {
        return $this->getAll($this->table, $this->columns);
    }

    public function getOneBySlug($slug)
    {
        return $this->getPageBySlug($this->table, $slug);
    }

    public function remove($data)
    {
        if (fileExist($data['url_to_delete'])) {
            unlink("application/views/page/users-pages/" . $data['url_to_delete'] . ".php");
        }

        $this->deleteRecord($this->table, $data['url_to_delete']);
    }
}